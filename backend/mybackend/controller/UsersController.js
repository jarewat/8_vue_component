const User = require('../models/User')

const userContrller = {
  addUser (req, res, next) {
    const payload = req.body
    const user = new User(payload)
    user
      .save()
      .then(() => {
        res.json(user)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateUser (req, res, next) {
    const { id } = req.params
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  deleteUser (req, res, next) {
    const { id } = req.params
    User.deleteOne({ _id: id })
      .then(() => {
        res.json({ id })
      })
      .catch(err => {
        res.status(500).send(error)
      })
    // res.json(usersContrller.deleteUser(id))
  },
  getUsers (req, res, next) {
    User.find({}, { name: 1, gender: 1 })
      .then(users => {
        res.json(users)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  getUser (req, res, next) {
    const { id } = req.params
    User.findById(id, { name: 1, gender: 1 })
      .then(users => {
        res.json(users)
      })
      .catch(err => {
        res.status(500).send(err)
      })
    // res.json(usersContrller.getUser(id))
  }
}

module.exports = userContrller
