var express = require('express');
var router = express.Router();

/* GET home page with message. */
router.get('/:message', function (req, res, next) {
  const { params } = req

  res.json({
    message: 'Hello',
    params
  })
});
/* GET home page. */
router.get('/', function (req, res, next) {
  const { params } = req

  res.json({
    message: 'Kam so cute!!!',
  })
});

module.exports = router;
